/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt8e20aldarias;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fichero: Ejercicio0802.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-feb-2014
 */
public class Ejercicio0802 {

  public static void main(String args[])
          throws IOException {
    char c;

    //Crea BufferedReader vinculado a System.in
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.println("Escriba caracteres. Punto para terminar:");

    // Lee caracteres
    do {
      c = (char) br.read();
      System.out.print(c);
    } while (c != '.');
  }
}
/* EJECUCION
 Escriba caracteres. Punto para terminar:
 a
 a
 b
 b
 .
 .
 */
