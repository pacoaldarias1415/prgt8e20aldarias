/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt8e20aldarias;

import java.io.FileReader;
import java.io.FileWriter;

/**
 * Fichero: Ejercicio0805.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-feb-2014
 */
public class Ejercicio0805 {

  public static void main(String args[]) throws Exception {
    FileReader fr = new FileReader("fin.txt");
    FileWriter fw = new FileWriter("fout.txt");
    char c;
    c = (char) fr.read();
    while (c != (char) -1) {
      fw.write(c);
      System.out.println(c + " Caracter Unicode: " + (int) c);
      c = (char) fr.read();
    }
    fr.close();
    fw.close();
  }
}
